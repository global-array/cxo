<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Model\Activities;
use App\Model\Events;
use App\Model\Participant;
use App\Model\Sponsor_request;
use Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $participated = Participant::where('user_id', Auth::id())->with('event')->get();
        $events = Events::where('approval', 1)->orderBy('date', 'desc')->get();
        return view('home')->with(compact('events', 'participated'));
    }

    public function profile(){
        $activities = Activities::where('user_id', Auth::id())->orderBy('created_at', 'desc')->get();
        return view('profile')->with(compact('activities'));
    }

    function validateApplication(Request $request, $id){
        $rules = [
            'appointment' => 'required',
            'validid' => 'required|max:5120|image',
        ];

        $messages = [
            'appointment.required' => 'A appointment is required',
            'validid.required' => 'A Valid ID is required',
            'validid.image' => 'A Valid ID must an image file',
        ];
        
        $validator = Validator::make( $request->all(), $rules, $messages );
        
        try{
            if($validator->passes()){
                if(Input::hasFile('validid')){
                    $res = $this->application($request, $id);
                    return $res;
                }else{
                    return response()->json(['error'=>['invalid image']]);
                }
            }
        }catch(\Exception $e){
            return response()->json(['error'=>[$e->getMessage()]]);
        }
    }

    function application($request, $id){
        $checkRequest = Sponsor_request::where('user_id', Auth::id())->first();
        // return $checkRequest;
        if($checkRequest){
            return response()->json([
                'message' => 'Request Already Sent!',
                'status' => false
            ]);
        }else{
            $input = $request->all();
            $request = new Sponsor_request;
            $request->user_id = $id;
            $request->status = 0;
            $request->image = $input['validid']->getClientOriginalName();
            $request->appointment = $input['appointment'];
            if($request->save()){
                $input['validid']->move('img/valid_id/'.Auth::id().'/', $input['validid']->getClientOriginalName());
                
                $act = new Activities;
                $act->user_id = Auth::id();
                $act->activities = 'You have requested to be a sponsor';
                $act->save();
                return response()->json([
                    'message' => 'Sponsor request sent!',
                    'status' => 201
                ]);
            }
        }
    }
}
