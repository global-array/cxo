<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Sponsor_request;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function index(){
        // $requests = Sponsor_request::where('status', 0)->count();
        // $users = User::all();
        // return view('admin.users')->with(compact('users', 'requests'));
        return view('admin.users');
    }

    function pendingUsers(){
        // $users = User::where('permission', 0)->get();
        $requests = Sponsor_request::where('status', 0)->get();
        return view('admin.pending-users')->with(compact('requests'));
    }

    function approval(Request $request, $id){
        $input = $request->all();

        $request = Sponsor_request::find($id);
        $request->status = $input['action'];
        if($request->save()){
            if($input['action'] == 1){
                $user = User::find($request->user_id);
                $user->permission = $input['action'];
                $user->save();
                return response()->json([
                    'message' => 'New Sponsor Added!',
                    'status' => true
                ]);
                // return redirect('admin/users')->with('status', 'New Sponsor Added!');
            }
            
            return response()->json([
                'message' => 'Sponsor request denied!',
                'status' => false
            ]);
            // return redirect('admin/users')->with('status', 'Sponsor request denied!');
        }
    }
}
