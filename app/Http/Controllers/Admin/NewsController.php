<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\News;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin')->except('getNews');
    }

    function index(){
        return view('admin.news');
    }

    function getNews(){
        return News::orderBy('created_at', 'desc')->limit(3)->get();
    }
}
