<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\ApprovedEvent;
use App\Model\Events;
use App\Model\Participant;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    function index(){
        $data = 'somethings';
        return view('admin.events');
    }

    function newEvent(){
        return view('admin.new-event');
    }
    
    public function eventParticipant(){
        return view('admin.event-participant');
    }
    
    public function showEvent($id){
        $event = Events::find($id);
        return view('admin.show-event')->with(compact('event'));
    }

    // function reportView(){
    //     return view('admin.report');
    // }
    
    function eventReport($id){
        $event = Events::find($id);
        $participants = Participant::where('event_id', $id)->where('participate_request', 1)->with('user')->get();
        $attended = Participant::where('event_id', $id)->where('participate_request', 1)->where('attendance', 1)->count();
        $absent = Participant::where('event_id', $id)->where('participate_request', 1)->where('attendance', '!=', 1)->count();
        return view('admin.report')->with(compact('event','participants','attended','absent'));
    }
}
