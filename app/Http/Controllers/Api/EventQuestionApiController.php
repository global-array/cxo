<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Model\Participant;
use App\Model\Answers;
use App\Model\Events;
use App\Model\Activities;

class EventQuestionApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    function check_participation($id){
        return Participant::where('user_id', Auth::id())->where('event_id', $id)->first();
    }

    function getParticipants($id){
        return Participant::where('event_id', $id)->count();
    }

    function submitAnswers(Request $request, $id){
        $participation = $this->check_participation($id);
        if($participation){
            return response()->json([
                'message' => 'You have already submitted a request.',
                'status' => 400
            ]);
        }
        $maxparticipants = $this->getParticipants($id);
        $max = Events::find($id);
        if($maxparticipants >= $max->max_participants){
            return response()->json([
                'message' => 'Max Participants count has been reached.',
                'status' => 400
            ]);
        }
        
        $input = $request->all();
        $participate = new Participant;
        $participate->event_id = $id;
        $participate->user_id = Auth::id();
        $participate->participate_request = 2;
        if($participate->save()){
            foreach($input as $row){
                $ans = new Answers;
                $ans->question_id = $row['question_id'];
                $ans->user_id = Auth::id();
                $ans->answers = $row['answer'];
                $ans->save();
            }
            
            $event = Events::find($participate->event_id);
            $act = new Activities;
            $act->user_id = Auth::id();
            $act->activities = 'You have requested to join in event named '.$event->title;
            $act->save();
            return response()->json([
                'message' => 'Join Request has been sent!',
                'status' => 201
            ]);
        }
    }

    function participated_event(){
        return Participant::where('user_id', Auth::id())->get();
    }
}
