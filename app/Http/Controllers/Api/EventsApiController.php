<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Events\UpdateEventsEvent;
use App\Events\ParticipantsEvent;
use App\Mail\EventJoinMail;
use App\Model\Event_questions;
use App\Model\Events;
use App\Model\Participant;
use App\Model\Activities;
use App\User;
use QrCode;
use Image;
use Carbon\Carbon;

class EventsApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin-api')->except('allEvents', 'requestEvent');
    }

    function allEvents(){
        // return Events::where('approval', 1)->where('date', '>', date("m/d/Y"))->with('participants', 'questionaire', 'questionaire.answer')->orderBy('date', 'desc')->get();
        return Events::where('approval', 1)->with('participants', 'questionaire', 'questionaire.answer')->orderBy('date', 'desc')->get();
    }
    
    function pendingEvents(){
        return Events::where('approval', 0)->with('created_by', 'questionaire')->get();
    }

    function participants($id){
        return Participant::where('event_id', $id)->where('participate_request', 2)->with('user')->get();
    }

    function approvedParticipants($id){
        return Participant::where('event_id', $id)->where('participate_request', 1)->with('user')->get();
    }
    
    function removeQuestion($id){
        $question = Event_questions::find($id);
        if($question->delete()){
            $pendings = Events::where('id', $question->event_id)->with('created_by', 'questionaire')->first();
            $act = new Activities;
            $act->user_id = $pendings->created_by;
            $act->activities = 'The question that you submitted on Event titled: '.$pendings->title.' is inapproriate the admin had removed it.';
            $act->save();
            return $pendings;
        }
    }

    function checkQR(Request $request){
        $input = $request->all();
        // return $input;
        $id = substr($input['qrcode'], 0, -40);
        $participant = Participant::where('event_id', $input['event_id'])->where('qrcode', $input['qrcode'])->firstOrFail();
        // return $participant;
        if($participant->attendance != 1){            
            $participant->attendance = 1;
            if($participant->save()){
                $event = Events::find($participant->event_id);
                $act = new Activities;
                $act->user_id = $participant->user_id;
                $act->activities = 'You have attended in event named '.$event->title;
                $act->save();
                $data = $this->approvedParticipants($input['event_id']);
                $broadcast = Participant::where('id', $participant->id)->with('user')->first();
                broadcast(new ParticipantsEvent($broadcast));
                return response()->json([
                    'data' => $data,
                    'status' => 200,
                    'message' => 'QR Code Verfied!'
                ]);
            }
        }else{
            return response()->json([
                'data' => $participant,
                'status' => 203,
                'message' => 'QR Code already used!'
            ]);
        }
    }

    function participateRequest(Request $request, $id){
        $participant = Participant::where('id', $id)->with('user')->first();
        $user_id = User::find($participant->user_id);
        $qrdata = $participant->id.str_random(40);
        // $user_id = bcrypt(User::find($participant->user_id).str_random(40));
        $image = 'data:image/png;base64,'.base64_encode(QrCode::format('png')->color(38, 38, 38, 0.85)->backgroundColor(255, 255, 255, 0.82)->size(200)->generate($qrdata));
        $name = 'event-qr'.$participant->id.'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
        $result = Image::make($image)->save(public_path('img/qrcode/').$name);        
        $participant->participate_request = $request['action'];
        $participant->qrcode = $qrdata;
        $participant->qrimage = 'event-qr'.$participant->id.'.png';
        if($participant->save()){
            $email = User::find($participant->user_id);
            $event = Events::find($participant->event_id);
            if($request['action'] == 1){
                $act = new Activities;
                $act->user_id = $user_id->id;
                $act->activities = 'Your requested to join in event named '.$event->title.' has been Approved!';
                $act->save();
                Mail::to($email->email)->send(new EventJoinMail($participant));
                return response()->json([
                    'message' => 'Request Approved!',
                    'status' => 201,
                    'data' => $participant
                ]);
            }else{         
                $act = new Activities;
                $act->user_id = $user_id->id;
                $act->activities = 'Your requested to join in event named '.$event->title.' has been Denied!';
                $act->save();       
                return response()->json([
                    'message' => 'Request Denied!',
                    'status' => 201,
                    'data' => $participant
                ]);
            }
        }
    }

    function requestEvent(Request $request){
        $input = $request->all();
        $reg = explode (",", $input['registrationdate']);
        if($reg[0]>$input['eventdate'] || $reg[1]>$input['eventdate']){
            return response()->json([
                'message' => 'Invalid Registration Dates',
                'status' => 403,
            ]);
        }else{
            $event = new Events;
            $event->title = $input['eventname'];
            $event->description = $input['description'];
            $event->date = $input['eventdate'];
            $event->image = $input['eventimage']->getClientOriginalName();
            $event->venue = $input['eventlocation'];
            $event->max_participants = $input['eventmax'];
            $event->registration_from = $reg[0];
            $event->registration_to = $reg[1];
            $event->approval = 0;
            $event->request_sponsor = 0;
            $event->created_by = Auth::id();
            if($event->save()){
                $act = new Activities;
                $act->user_id = Auth::id();
                $act->activities = 'You have requested a event named '.$input['eventname'];
                $act->save();
                $q = json_decode($input['questionaire'], true);
                foreach($q as $data){
                    $question = new Event_questions;
                    $question->event_id = $event->id;
                    $question->question = $data['question'];
                    $question->save();
                }
                $input['eventimage']->move('img/events/', $input['eventimage']->getClientOriginalName());
                return response()->json([
                    'message' => 'Event request sent!',
                    'status' => 200,
                ]);
            }else{
                return response()->json([
                    'message' => 'Fail to send request!',
                    'status' => 403,
                ]);
            }
        }
    }

    function approval(Request $request, $id){
        $input = $request->all();

        $event = Events::find($id);
        $event->approval = $input['action'];
        if($event->save()){
            if($input['action'] == 1){
                $res = Events::where('id', $event->id)->where('approval', 1)->with('participants', 'questionaire', 'questionaire.answer')->first();
                broadcast(new UpdateEventsEvent($res))->toOthers();
                $pendings = Events::where('approval', 0)->with('created_by')->get();
                
                $act = new Activities;
                $act->user_id = $event->created_by;
                $act->activities = 'Your requested event named '.$res->title.' has been approved!';
                $act->save();
                return response()->json([
                    'message' => 'Event has been approved!',
                    'pendings' => $pendings,
                    'data' => $res,
                    'status' => true
                ]);
                // return redirect('admin/users')->with('status', 'New Sponsor Added!');
            }
            
            $act = new Activities;
            $act->user_id = $event->created_by;
            $act->activities = 'Your requested event named '.$res->title.' has been denied!';
            $act->save();
            return response()->json([
                'message' => 'Event request denied!',
                'status' => false
            ]);
            // return redirect('admin/users')->with('status', 'Sponsor request denied!');
        }
    }
}
