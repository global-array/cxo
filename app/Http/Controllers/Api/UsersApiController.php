<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\UpdateUserEvent;
use App\Model\Sponsor_request;
use App\Model\Activities;
use App\User;

class UsersApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin-api');
    }

    function allUsers(){
        return User::all();
    }

    function pendingUsers(){
        // $users = User::where('permission', 0)->get();
        $pending = Sponsor_request::where('status', 0)->with('user')->get();
        return $pending;
    }

    function changeType(Request $request, $id){
        $input = $request->all();
        
        $user = User::find($id);
        $user->permission = $input['userType'];
        if($user->save()){                
            return response()->json([
                'message' => 'User Type has been Updated!',
                'data' => $user,
                'status' => 201
            ]);
        }
    }
    
    function restoreUser($id){
        User::withTrashed()->find($id)->restore();
        $user = User::find($id);
        if($user){
            return response()->json([
                'data' => $user,
                'status' => 200,
                'message' => 'A User has been restore!'
            ]);
        }
    }
    
    function usersArchived(){
        return User::onlyTrashed()->get();
    }

    function removeUser($id){
        $user = User::find($id);
        if($user->delete()){
            return response()->json([
                'data' => $user,
                'status' => 200,
                'message' => 'A User has been removed!'
            ]);
        }
    }

    function approval(Request $request, $id){
        $input = $request->all();
        $request = Sponsor_request::where('user_id', $id)->first();
        $request->status = $input['action'];
        if($request->save()){
            if($input['action'] == 1){
                $user = User::find($request->user_id);
                $user->permission = $input['action'];
                $user->save();
                
                $act = new Activities;
                $act->user_id = $request->user_id;
                $act->activities = 'Your sponsor request has been approved!';
                $act->save();
                $pending = Sponsor_request::where('status', 0)->with('user')->get();
                broadcast(new UpdateUserEvent($user))->toOthers();
                return response()->json([
                    'message' => 'New Sponsor Added!',
                    'data' => $user,
                    'pendings' => $pending,
                    'status' => 201
                ]);
            }
            
            $act = new Activities;
            $act->user_id = $request->user_id;
            $act->activities = 'Your sponsor request has been denied!';
            $act->save();
            return response()->json([
                'message' => 'Sponsor request denied!',
                'data' => [],
                'status' => 403
            ]);
        }
    }
}
