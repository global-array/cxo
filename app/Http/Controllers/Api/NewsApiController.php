<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Model\News;

class NewsApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin-api');
    }

    function allNews(){
        return News::all();
    }

    function addNews(Request $request){
        $input = $request->all();
        $news = new News;
        $news->title = $input['news_title'];
        $news->body = $input['body'];
        $news->image = $input['image']->getClientOriginalName();
        $news->created_by = Auth::id();
        if($news->save()){
            $input['image']->move('img/news/', $input['image']->getClientOriginalName());
            return response()->json([
                'data' => $news,
                'status' => 200,
                'message' => 'A News has been added!'
            ]);
        }
    }

    function restoreNews($id){
        News::withTrashed()->find($id)->restore();
        $news = News::find($id);
        if($news){
            return response()->json([
                'data' => $news,
                'status' => 200,
                'message' => 'A News has been restore!'
            ]);
        }
    }

    function newsArchives(){
        return News::onlyTrashed()->get();
    }

    function deleteNews($id){
        $news = News::find($id);
        if($news->delete()){
            return response()->json([
                'data' => $news,
                'status' => 200,
                'message' => 'A News has been set to archive!'
            ]);
        }
    }
}
