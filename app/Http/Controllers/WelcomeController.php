<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\News;

class WelcomeController extends Controller
{
    public function index(){
        $news = News::orderBy('created_at', 'desc')->limit(3)->get();
        return view('welcome')->with(compact('news'));
    }
}
