<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Model\Events;
use App\Model\Participant;
use Validator;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $participated = Participant::where('user_id', Auth::id())->get();
        $events = Events::where('approval', 1)->orderBy('date', 'desc')->paginate(2);
        return view('events.index')->with(compact('events', 'participated'));
    }

    function approvedEvents(){
        $events = Events::where('approval', 1)->orderBy('date', 'desc')->get();
        return response()->json($events);
    }
    
    public function newEvent(){
        if(Auth::user()->permission != 1){
            return redirect('events')->with('status', 'Permission Denied!');
        }
        return view('events.new-event');
    }

    function myEvents(){
        $events = Events::where('created_by', Auth::id())->get();
        return view('events.my-event')->with(compact('events'));
    }

    function validateEvent(Request $request){
        $rules = [
            'eventname' => 'required',
            'eventdescription' => 'required',
            'eventdate' => 'required',
            'eventimage' => 'required|max:5120|image',
            'eventlocation' => 'required',
            'eventmax' => 'required',
            'regstart' => 'required',
        ];

        $messages = [
            'eventname.required' => 'A Event Name is required',
            'eventdescription.required'  => 'A Event Description is required',
            'eventdate.required' => 'A Event Date is required',
            'eventimage.required' => 'A Event Image is required',
            'eventimage.max:5120' => 'A Event Image must be below 5mb',
            'eventimage.image' => 'A Event Image must an image file',
            'eventlocation.required' => 'A Event Location is required',
            'eventmax.required' => 'A Number Participants is required',
            'regstart.required' => 'A Registration Date is required',
        ];
        
        $validator = Validator::make( $request->all(), $rules, $messages );
        
        try{
            if($validator->passes()){
                if(Input::hasFile('eventimage')){
                    $this->submitEvent($request);
                }else{
                    return response()->json(['error'=>['invalid image']]);
                }
            }
        }catch(\Exception $e){
            return response()->json(['error'=>[$e->getMessage()]]);
        }
        
        return response()->json(['error'=>$validator->errors()->all()]);
    }
    function requestEvent(Request $request){
        return $request;
    }

    function submitEvent($data){
        $input = $data->all();
        $event = new Events;
        $event->title = $input['eventname'];
        $event->description = $input['eventdescription'];
        $event->date = $input['eventdate'];
        $event->image = $input['eventimage']->getClientOriginalName();
        $event->venue = $input['eventlocation'];
        $event->max_participants = $input['eventmax'];
        $event->registration_from = $input['regstart'];
        $event->registration_to = $input['regend'];
        $event->approval = 0;
        $event->request_sponsor = 0;
        $event->created_by = Auth::id();
        if($event->save()){
            $input['eventimage']->move('img/events/', $input['eventimage']->getClientOriginalName());
            return response()->json([
                'message' => 'Event request sent!',
                'status' => 200,
            ]);
        }else{
            return response()->json([
                'message' => 'Fail to send request!',
                'status' => 403,
            ]);
        }
    }

    public function showEvent($id){
        // return redirect('events')->with('status', 'Event request sent!');
        $event = Events::find($id);
        return view('events.event')->with(compact('event'));
    }
}
