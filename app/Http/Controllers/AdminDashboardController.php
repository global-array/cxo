<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Model\Events;

class AdminDashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
        $totalusers = User::where('email_verified_at', '!=', null)->where('permission', '!=', 1)->count();
        $totalsponsors = User::where('email_verified_at', '!=', null)->where('permission', '=', 1)->count();
        $all = User::where('email_verified_at', '!=', null)->count();
        $events = Events::where('approval', 1)->orderBy('date', 'desc')->limit(3)->get();
        return view('admin-dashboard')->with(compact('totalusers', 'totalsponsors', 'all','events'));
    }

}
