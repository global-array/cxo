<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Participant extends Model
{
    // use SoftDeletes;

	protected $table = 'event_participants';
    public $primaryKey = 'id';
    public $timestamps = true;

    
	public function event(){
		return $this->hasOne('App\Model\Events', 'id' , 'event_id');
    }

    public function user(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
