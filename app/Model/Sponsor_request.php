<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Sponsor_request extends Model
{
	protected $table = 'sponsor_request';
    public $primaryKey = 'id';
    public $timestamps = true;

    
	public function user(){
		return $this->hasOne('App\User', 'id' , 'user_id');
    }
}
