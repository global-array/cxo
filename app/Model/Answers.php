<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Answers extends Model
{
	protected $table = 'answers';
    public $primaryKey = 'id';
    public $timestamps = true;

    
	public function question(){
		return $this->hasOne('App\Model\Event_questions', 'id' , 'question_id');
    }
}
