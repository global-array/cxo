<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Events extends Model
{
    use SoftDeletes;

	protected $table = 'events';
    public $primaryKey = 'id';
    public $timestamps = true;

	public function participants(){
		return $this->hasMany('App\Model\Participant', 'event_id' , 'id');
    }

    // public function participants(){
    //     return $this->hasMany('App\Model\Participant');
    // }

    // public function pending_request(){
    //     return $this->participants()->where('participate_request', 2)->where('event_id', 'id')->get();
    // }

    public function created_by(){
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function questionaire(){
        return $this->hasMany('App\Model\Event_questions', 'event_id', 'id');
    }
}
