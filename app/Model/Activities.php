<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Activities extends Model
{
	protected $table = 'activities';
    public $primaryKey = 'id';
    public $timestamps = true;
}
