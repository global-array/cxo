<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event_questions extends Model
{
	use SoftDeletes;
	
	protected $table = 'event_questions';
    public $primaryKey = 'id';
    public $timestamps = true;

    
	public function event(){
		return $this->hasOne('App\Model\Events', 'id' , 'event_id');
    }

	public function answer(){
		return $this->hasMany('App\Model\Answers', 'question_id' , 'id');
    }

}
