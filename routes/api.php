<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'users'], function(){
    Route::post('/pending/{id}', 'Api\UsersApiController@approval');
    Route::get('/pending', 'Api\UsersApiController@pendingUsers');
    Route::get('/archives/{id}', 'Api\UsersApiController@restoreUser');
    Route::get('/archives', 'Api\UsersApiController@usersArchived');
    Route::delete('/{id}', 'Api\UsersApiController@removeUser');
    Route::put('/{id}', 'Api\UsersApiController@changeType');
    Route::get('/', 'Api\UsersApiController@allUsers');
});

Route::group(['prefix' => 'news'], function(){
    Route::get('/archives/{id}', 'Api\NewsApiController@restoreNews');
    Route::get('/archives', 'Api\NewsApiController@newsArchives');
    Route::delete('/{id}', 'Api\NewsApiController@deleteNews');
    Route::post('/', 'Api\NewsApiController@addNews');
    Route::get('/', 'Api\NewsApiController@allNews');
});

Route::group(['prefix' => 'events'], function(){
    Route::post('/pending/{id}', 'Api\EventsApiController@approval');
    Route::post('/participants/request/{id}', 'Api\EventsApiController@participateRequest');
    Route::get('/participants/approved/{id}', 'Api\EventsApiController@approvedParticipants');
    Route::get('/participants/{id}', 'Api\EventsApiController@participants');
    Route::post('/check-qr', 'Api\EventsApiController@checkQR');
    Route::get('/pending', 'Api\EventsApiController@pendingEvents');
    Route::post('/request', 'Api\EventsApiController@requestEvent')->middleware('auth:api');
    Route::get('/participated', 'Api\EventQuestionApiController@participated_event');
    Route::delete('/question/{id}', 'Api\EventsApiController@removeQuestion');
    Route::get('/', 'Api\EventsApiController@allEvents');
});

Route::group(['prefix' => 'questions'], function(){
    Route::post('/{id}', 'Api\EventQuestionApiController@submitAnswers');
});