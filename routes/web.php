<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'WelcomeController@index');

Auth::routes(['verify' => true]);
Auth::routes();

Route::post('/apply-sponsor/{id}', 'HomeController@validateApplication');
Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');
Route::get('/profile', 'HomeController@profile')->name('profile')->middleware('verified');
Route::post('/profile', 'ChangePasswordController@changepassword');
Route::post('/show-email', function(){
	return view('email.join-mail');
});

Route::group(['prefix' => 'activities', 'middleware' => 'verified'], function(){
	Route::get('/', 'ActivityController@index')->name('activities');
});

Route::group(['prefix' => 'events', 'middleware' => 'verified'], function(){
	Route::get('/my-events', 'EventController@myEvents')->name('events.mine');
	Route::get('/new', 'EventController@newEvent')->name('events.new');
	Route::post('/new', 'EventController@validateEvent')->name('events.new.submit');
	Route::get('/{id}', 'EventController@showEvent');
	Route::get('/', 'EventController@index')->name('events');
});

Route::group(['prefix' => 'admin'], function(){
	Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
	Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
	Route::get('/', 'AdminDashboardController@index')->name('admin.dashboard');
	
	Route::get('/news', 'Admin\NewsController@index')->name('admin.news');
	Route::get('/users', 'Admin\UserController@index')->name('admin.users');
	Route::get('/events/{id}/participants', 'Admin\EventController@showParticipant');
	Route::get('/events/{id}', 'Admin\EventController@showEvent');
	Route::get('/events', 'Admin\EventController@index')->name('admin.events');
	
	Route::get('/event-participants', 'Admin\EventController@eventParticipant');
	
	Route::get('/reports/{id}', 'Admin\EventController@eventReport');
	// Route::get('/reports', 'Admin\EventController@reportView');
});


Route::get('/approvedEvents', 'EventController@approvedEvents');