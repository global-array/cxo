/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import lang from 'element-ui/lib/locale/lang/en';
import locale from 'element-ui/lib/locale';
import CKEditor from '@ckeditor/ckeditor5-vue';
import Chart from 'chart.js';
locale.use(lang);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));
import store from './vuex/store'

Vue.component('sponsor-request', require('./components/admin/users/SponsorRequestComponent.vue').default);
Vue.component('users-component', require('./components/admin/users/UsersComponent.vue').default);
Vue.component('news-component', require('./components/admin/news/NewsComponent.vue').default);
Vue.component('new-news-component', require('./components/admin/news/NewNewsComponent.vue').default);
Vue.component('events-component', require('./components/admin/events/EventsComponent.vue').default);
Vue.component('pending-event-component', require('./components/admin/events/PendingEventsComponent.vue').default);
Vue.component('participants-component', require('./components/admin/events/ParticipantsComponent.vue').default);
Vue.component('view-participant', require('./components/admin/events/ParticipantsViewsComponent.vue').default);
Vue.component('report-component', require('./components/admin/events/ReportsComponent.vue').default);

Vue.component('profile-component', require('./components/ProfileComponent.vue').default);
Vue.component('user-events-component', require('./components/events/EventsComponent.vue').default);
Vue.component('new-event-component', require('./components/events/NewEventsComponent.vue').default);
Vue.use( CKEditor );
Vue.use(ElementUI);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    store
});
