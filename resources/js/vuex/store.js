import Vue from 'vue';
import Vuex from 'vuex';

import userStore from './../components/admin/users/userStore';
import newsStore from './../components/admin/news/newsStore';
import eventStore from './../components/admin/events/eventStore';
import participateStore from './../components/events/participateStore';

Vue.use(Vuex);
Vue.config.debug = true;

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        userStore,
        newsStore,
        eventStore,
        participateStore
    },
    strict: debug
});