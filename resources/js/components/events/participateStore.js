import axios from 'axios'
// import Echo from 'laravel-echo';

const state = () => {
    return {
        participated_events: []
    };
};

const getters = {
    allParticipatedEvents: state => state.participated_events,
};

const actions = {
    async getEventsParticipated({ commit }, config){
        const response = await axios.get('/api/events/participated', config)

        commit('setParticipated', response.data)
        return response.data;
    },
};

const mutations = {
    setParticipated: (state, events) => (state.participated_events = events),
    // setPendingEvent: (state, pending_events) => (state.pending_events = pending_events),
    // updEvent: (state, updEvent) => state.events.push(updEvent),
    // updEvent: (state, updEvent) => {
    //     const index = state.events.findIndex(event => event.id === updEvent.id);
    //     if (index !== -1) {
    //         state.events.splice(index, 1, updEvent);
    //     }
    // }
};

export default {
    state,
    getters,
    actions,
    mutations
};