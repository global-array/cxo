import axios from 'axios'
// import Echo from 'laravel-echo';

const state = () => {
    return {
        users: [],
        sponsor_requests: 0,
        archived_users: []
    };
};

const getters = {
    allUsers: state => state.users,
    allRequests: state => state.sponsor_requests,
    allArchivedUsers: state => state.archived_users,
};

const actions = {
    async fetchUsers({ commit }, config){
        const response = await axios.get('/api/users', config)

        commit('setUsers', response.data)
    },

    async fetchSponsorRequest({ commit }, config){
        const response = await axios.get('/api/users/pending', config)

        commit('setSponsorsRequest', response.data)
    },

    async updateUser({ commit }, user){
        const response = await axios.post('/api/users/pending/'+user.id , user.values, user.config)

        commit('updUser', response.data.data)
        commit('setSponsorsRequest', response.data.pendings)
        
        return response.data;
    },

    async updateType({commit}, user){
        const response = await axios.put('/api/users/'+user.id, user.values, user.config)

        commit('updUser', response.data.data)
        return response.data
    },

    async fetchArchivedUser({commit}, config){
        const response = await axios.get('/api/users/archives', config)

        commit('setArchivedUsers', response.data)

        return response
    },

    async restoreUser({commit}, user){
        const response = await axios.get('/api/users/archives/'+user.id, user.config)

        commit('newUser', response.data.data)
        commit('dltArchive', response.data.data)

        return response.data
    },

    async removeUser({commit}, user){
        const response = await axios.delete('/api/users/'+user.id, user.config)
        console.log(response)
        commit('dltUser', response.data.data)
        commit('userArchives', response.data.data)

        return response
    }
};

const mutations = {
    setUsers: (state, users) => (state.users = users),
    setSponsorsRequest: (state, sponsor_requests) => (state.sponsor_requests = sponsor_requests),
    setArchivedUsers: (state, archived_users) => (state.archived_users = archived_users),
    newUser: (state, user) => state.users.push(user),
    userArchives: (state, user) => state.archived_users.push(user),
    updUser: (state, updUser) => {
        const index = state.users.findIndex(user => user.id === updUser.id);
        if (index !== -1) {
            state.users.splice(index, 1, updUser);
        }
    },    
    dltArchive: (state, dltArchive) => {
        const index = state.archived_users.findIndex(archived_users => archived_users.id === dltArchive.id);
        if (index !== -1) {
            state.archived_users.splice(index, 1);
        }
    },
    dltUser: (state, dltUser) => {
        const index = state.users.findIndex(user => user.id === dltUser.id);
        if (index !== -1) {
            state.users.splice(index, 1);
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};