import axios from 'axios'
// import Echo from 'laravel-echo';

const state = () => {
    return {
        events: [],
        pending_events: [],
        event_participants: [],
        approved_participants: [],
    };
};

const getters = {
    allEvents: state => state.events,
    allPendingEvents: state => state.pending_events,
    eventById: state => (id) => {
        return state.pending_events.find(event => event.id === id)
    },
    eventParticipants: state => state.event_participants,
    approvedParticipants: state => state.approved_participants,
};

const actions = {
    async fetchEvents({ commit }, config){
        const response = await axios.get('/api/events', config)
        // console.log(response.data)
        commit('setEvents', response.data)
    },

    async fetchPendingEvents({commit}, config){
        const response = await axios.get('/api/events/pending', config)

        commit('setPendingEvent', response.data)
    },

    async fetchEventParticipants({commit}, event){
        const response = await axios.get('/api/events/participants/'+event.id, event.config)

        commit('setEventParticipants', response.data)
        return response.data
    },

    async fetchApprovedParticipants({commit}, event){
        const response = await axios.get('/api/events/participants/approved/'+event.id, event.config)
        commit('setApprovedParticipants', response.data)
        return response.data
    },

    async checkQR({commit}, participant){
        const response = await axios.post('/api/events/check-qr', participant.values, participant.config)

        commit('updParticipants', response.data.data)
        // console.log(response);
        return response
    },

    async updateParticipants({commit}, event){
        const response = await axios.post('/api/events/participants/request/'+event.id, event.values, event.config)

        // commit('fetchEvents', event.data)
        commit('setPendingEvent', response.data)
        return response.data
    },

    async updateEvent({commit}, event){
        const response = await axios.post('/api/events/pending/'+event.id, event.values, event.config)
        // console.log(response.data)
        commit('addEvent', response.data.data)
        commit('setPendingEvent', response.data.pendings)

        return response.data
    },

    async removeQuestion({commit}, question){
        const response = await axios.delete('/api/events/question/'+question.id, question.config)
        // console.log(response.data)
        commit('updEvent', response.data)
        return response.data
    }
};

const mutations = {
    setEvents: (state, events) => (state.events = events),
    setPendingEvent: (state, pending_events) => (state.pending_events = pending_events),
    setEventParticipants: (state, event_participants) => (state.event_participants = event_participants),
    addEvent: (state, updEvent) => state.events.push(updEvent),
    setApprovedParticipants: (state, approved_participants) => (state.approved_participants, approved_participants),
    updParticipants: (state, participant) => {
        const index = state.approved_participants.findIndex(approved_participant => approved_participant.id === participant.id);
        if (index !== -1) {
            state.approved_participants.splice(index, 1, participant);
        }
    },
    updEvent: (state, updEvent) => {
        const index = state.pending_events.findIndex(event => event.id === updEvent.id);
        if (index !== -1) {
            state.pending_events.splice(index, 1, updEvent);
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};