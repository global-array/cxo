import axios from 'axios'
// import Echo from 'laravel-echo';

const state = () => {
    return {
        news: [],
        archives: []
    };
};

const getters = {
    allNews: state => state.news,
    allArchives: state => state.archives
};

const actions = {
    async fetchNews({ commit }, config){
        const response = await axios.get('/api/news', config)

        commit('setNews', response.data)
        return response.data;
    },

    async addNews({commit}, news){
        const response = await axios.post('/api/news', news.values, news.config)

        commit('newNews', response.data.data)

        return response
    },

    async newsArchives({commit}, config){
        const response = await axios.get('/api/news/archives', config)

        commit('setArchives', response.data)

        return response
    },

    async restoreNews({commit}, news){
        const response = await axios.get('/api/news/archives/'+news.id, news.config)

        commit('newNews', response.data.data)
        commit('dltArchive', response.data.data)

        return response.data
    },

    async removeNews({commit}, news){
        const response = await axios.delete('/api/news/'+news.id, news.config)

        commit('dltNews', response.data.data)
        commit('newArchive', response.data.data)

        return response
    }
};

const mutations = {
    setNews: (state, news) => (state.news = news),
    setArchives: (state, archives) => (state.archives = archives),
    newNews: (state, news) => state.news.push(news),
    newArchive: (state, archives) => state.archives.push(archives),
    // setPendingEvent: (state, pending_events) => (state.pending_events = pending_events),
    // updEvent: (state, updEvent) => state.events.push(updEvent),
    dltArchive: (state, dltArchive) => {
        const index = state.archives.findIndex(archives => archives.id === dltArchive.id);
        if (index !== -1) {
            state.archives.splice(index, 1);
        }
    },
    dltNews: (state, dltNews) => {
        const index = state.news.findIndex(news => news.id === dltNews.id);
        if (index !== -1) {
            state.news.splice(index, 1);
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};