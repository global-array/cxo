@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <div class="row">
                        {{-- <div class="col-md-6 col-sm-12 col-lg-6 pt-3">
                            <h3>Upcoming Events</h3>
                            <div class="list-group"></div>
                        </div> --}}
                        <div class="col-md-12 col-sm-12 col-lg-12 pt-3">
                            <h3>Events Participation Status</h3>
                            <table class="table" id="paticipations_table">
                                <thead>
                                    <tr>
                                        <th scope="col">Event Title</th>
                                        <th scope="col">Date of Event</th>
                                        <th scope="col">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($participated) > 0)
                                        @foreach($participated as $row)
                                            <tr>
                                                <td>{{$row->event->title}}</td>
                                                <td>{{$row->event->date}}</td>
                                                <td style="font-size: 20px;">
                                                    @if($row->participate_request == 1)
                                                        <span class="badge badge-success">Approved</span>
                                                    @elseif($row->participate_request == 2)
                                                        <span class="badge badge-secondary">Pending</span>
                                                    @else
                                                        <span class="badge badge-danger">Deny</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>
    function loadevents(){
        axios.get('/approvedEvents')
        .then(res=>{
            const events = res.data;
            $('.list-group').empty();
            events.forEach(function(event){
                $('.list-group').append(`<a href="/events/${event.id}" class="list-group-item d-flex justify-content-between align-items-center">${event.title}
                    <span class="badge badge-primary badge-pill">${event.date}</span>
                </a>
                `);
            });
        })
        .catch(err=>{
            console.log(err)
        })
    }

    $(document).ready( function () {
        loadevents();
        $('#paticipations_table').DataTable();
        
        var channel = Echo.channel('approved-event');
            channel.listen('ApprovedEvent', function(data) {
            if(data.status){
                toastr.success(data.message)
                loadevents();
            }
        });
    } );
</script>
@endsection