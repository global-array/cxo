@extends('layouts.app')

@section('css')
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header h2">{{$event->title}}</div>
                <div class="card-body">
                    <img src="../img/events/{{$event->image}}" class="card-img-top img-fluid pb-3" alt="Responsive image">
                    <h4 class="card-title">Date: {{date('M d, Y', strtotime($event->date))}}</h4>
                    <h4 class="card-title">Location: {{$event->venue}}</h4>
                    <p class="card-text h5" id="description"></p>
                    <button class="btn btn-primary btn-block">REGISTER NOW</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function(){
        var description = document.getElementById('description');
        description.innerHTML = {!! json_encode($event->description) !!}
    })
</script>
@endsection