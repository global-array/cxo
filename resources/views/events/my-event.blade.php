@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header h2">
                    My Events
                    @if(Auth::user()->permission == 1)
                        <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">
                        Request Event
                        </button>

                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
                                    <new-event-component :user-auth="{{Auth::user()}}"></new-event-component>
                            </div>
                        </div>
                        <a href="{{route('events.mine')}}" class="btn btn-primary float-right" style="margin-right: 1%">Your Events</a>
                    @endif
                </div>
                <div class="card-body">
                    <table class="table" id="paticipations_table">
                        <thead>
                            <tr>
                                <th scope="col">Event Title</th>
                                <th scope="col">Date of Event</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($events) > 0)
                                @foreach($events as $row)
                                    <tr>
                                        <td>{{$row->title}}</td>
                                        <td>{{$row->date}}</td>
                                        <td style="font-size: 20px;">
                                            @if($row->approval == 1)
                                                <span class="badge badge-success" style="font-size: 90%;">Approved</span>
                                            @elseif($row->approval == 2)
                                                <span class="badge badge-danger" style="font-size: 90%;">Deny</span>
                                            @else
                                                <span class="badge badge-secondary" style="font-size: 90%;">Pending</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
@endsection