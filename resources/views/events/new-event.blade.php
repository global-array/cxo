@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header h2">
            Request Event
            <a class="btn btn-primary float-right" href="{{route('events')}}">Back</a>
        </div>
        <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="alert alert-danger d-none" role="alert"><ul></ul></div>
        
            <div class="row">
                <div class="col-md-12">                    
                    <div class="form-group">
                        <label for="eventname">Event Name</label>
                        <input type="text" class="form-control" id="eventname" placeholder="Event Name" />
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="eventlocation">Event Location</label>
                        <input type="text" class="form-control" id="eventlocation" name="eventlocation" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="eventdate">Event Date</label>
                        <input type="text" class="form-control" id="eventdate" name="eventdate" value="" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="eventmax">Max Participants</label>
                        <input type="number" class="form-control" id="eventmax" name="eventmax"/>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="registrationDates">Registration Dates</label>
                        <input class="form-control" id="registrationDates" type="text" name="registrationDates" value="" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="eventimage">Upload event image</label>
                        <input type="file" class="form-control-file" id="eventimage" accept='image/*' />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="editor">Event Details</label>
                        <div id="editor" style="height: 300px"></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button class="btn btn-primary float-right" onclick="submitEvent()" id="btn-submit">Save</button>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script>
    var quill = new Quill('#editor', {
        theme: 'snow'
    });
    var startdate = '';
    var enddate = '';

    $('input[name="registrationDates"]').daterangepicker({
        autoUpdateInput: true,
        locale: {
            cancelLabel: 'Clear'
        }
    });

    $('input[name="registrationDates"]').on('apply.daterangepicker', function(ev, picker) {
        startdate = picker.startDate.format('MM/DD/YYYY');
        enddate = picker.endDate.format('MM/DD/YYYY');
    });

    $('input[name="registrationDates"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $('input[name="eventdate"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
    });

    function submitEvent(){
        $('#btn-submit').attr('disabled', true);
        $('.alert ul').empty();
	    let eventimage = document.querySelector( '#eventimage' );
        let form = new FormData();
        form.append('_token', $('input[name=_token]' ).val());
        form.append('eventname', $('#eventname').val());
        form.append('eventlocation', $('#eventlocation').val());
        form.append('eventdate', $('#eventdate').val());
        form.append('eventmax', $('#eventmax').val());
        form.append('regstart', startdate);
        form.append('regend', enddate);
        form.append('eventimage', eventimage.files[0]);
        form.append('eventdescription', quill.root.innerHTML);

        axios.post('/events/new', form)
        .then(function(res){
            if($.isEmptyObject(res.data.error)){
                if(res.data.status == 200){
                    toastr.success(res.data.message)
                    setTimeout(() => {
                        window.location = '/events'
                    }, 3000);
                }else{
                    toastr.error(res.data.message)
                }
            }else{
                $('.alert.alert-danger').removeClass('d-none');
                var errors = res.data.error;
                errors.forEach(function(error){
                    $('.alert ul').append('<li>'+error+'</li>')
                })
                
                $('#btn-submit').attr('disabled', false);
                console.log(res);
            }
        })
        .catch(function(error){
            console.log(error);
        });
    }
</script>
@endsection