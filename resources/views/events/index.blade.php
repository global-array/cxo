@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span class="h2">Events</span>
                    @if(Auth::user()->permission == 1)
                        <button type="button" class="btn btn-primary float-right" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#exampleModal">
                        Request Event
                        </button>

                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
                                    <new-event-component :user-auth="{{Auth::user()}}"></new-event-component>
                            </div>
                        </div>
                        <a href="{{route('events.mine')}}" class="btn btn-primary float-right" style="margin-right: 1%">Your Events</a>
                    @endif
                </div>
                <div class="card-body">
                    <user-events-component :user-auth="{{Auth::user()}}"></user-events-component>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
