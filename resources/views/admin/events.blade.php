@extends('layouts.main')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header h2">
            Events
            <div class="float-right d-flex">
                <report-component :user-auth="{{Auth::user()}}"></report-component>
                <pending-event-component :user-auth="{{Auth::user()}}"></pending-event-component>
            </div>
        </div>
        <div class="card-body">
            <events-component :user-auth="{{Auth::user()}}"></events-component>
        </div>
    </div>
</div>
@endsection