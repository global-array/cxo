@extends('layouts.main')

@section('css')
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header h2">
                    {{$event->title}}
                    <participants-component :user-auth="{{Auth::user()}}" :event-data="{{$event}}"></participants-component>
                </div>
                <div class="card-body">
                    <img src="../../img/events/{{$event->image}}" style="height: 300px; background-repeat: no-repeat;" class="card-img-top img-fluid pb-3" alt="{{$event->image}}">
                    <h4 class="card-title">Date: {{date('M d, Y', strtotime($event->date))}}</h4>
                    <h4 class="card-title">Location: {{$event->venue}}</h4>
                    <p class="card-text h5" id="description"></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('js')
<script>
    $(document).ready(function(){
        // console.log({!! json_encode(Auth::user()->api_token) !!})
        var description = document.getElementById('description');
        description.innerHTML = {!! json_encode($event->description) !!}
    })
</script>
@endsection