@extends('layouts.main')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header h2">
            News
            <div class="float-right">
                <new-news-component :user-auth="{{Auth::user()}}"></new-news-component>
            </div>
        </div>
        <div class="card-body">
            <news-component :user-auth="{{Auth::user()}}"></news-component>
        </div>
    </div>
</div>
@endsection