<!DOCTYPE html>
<html>
<head>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- <link href="{{ asset('css/now-ui-dashboard.min.css') }}" rel="stylesheet"> -->
    <title>CXO Report</title>
</head>
<body class="text-dark">
    <div class="container py-5">
        <div class="card">
            <div class="card-body">
                <div class="media">
                    <img src="{{asset('img/cxo_square.png')}}" class="mx-3" alt="cxo_logo.png" width="10%">
                    <div class="media-body  text-center">
                        <h2 class="mt-4">
                            CXO PROJECT<br>
                            <small class="text-muted">Attendance Report</small>
                        </h2>
                        <h3 class="pt-2">{{$event->title}}</h3>
                    </div>
                </div>
                <div class="mt-5">
                    <table class="table table-bordered h5">
                        <thead>
                            <tr>
                                <th scope="col">First</th>
                                <th scope="col">Last</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($participants) > 0)
                                @foreach($participants as $participant)
                                    <tr>
                                        <td>{{$participant->user->firstname}}</td>
                                        <td>{{$participant->user->lastname}}</td>
                                        <td>@mdo</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="mt-5">
                    <table class="table table-bordered h5">
                        <tbody>
                            <tr>
                                <td>Total Number of Guest</td>
                                <td>{{count($participants)}}</td>
                            </tr>
                            <tr>
                                <td>Total Number of Guest Participated</td>
                                <td>{{$attended}}</td>
                            </tr>
                            <tr>
                                <td>Total Number of Guest did not participated</td>
                                <td>{{$absent}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>  -->
<script type="text/javascript">
	$(document).ready(function(){
		// window.print();
		// window.close();
	});
</script>

</body>
</html>