@extends('layouts.main')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header h2">
            Users
            <sponsor-request :user-auth="{{Auth::user()}}"></sponsor-request>
        </div>
        <div class="card-body">
            <users-component :user-auth="{{Auth::user()}}"></users-component>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script>
    $(document).ready( function () {
        $('#users_table').DataTable();
        
        const message =  {!! json_encode(session('status')) !!}
        if(message){
            toastr.success(message)
        }
    } );
</script>
@endsection