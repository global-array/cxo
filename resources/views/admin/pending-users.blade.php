@extends('layouts.main')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('css/all.css')}}">
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header h2">
            Pending Users
            <a class="btn btn-primary float-right" href="{{route('admin.users')}}">Back</a>
        </div>
        <div class="card-body">
            <table class="table" id="users_table">
                <thead>
                    <tr>
                        <th scope="col">Firstname</th>
                        <th scope="col">Lastname</th>
                        <th scope="col">Approval</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($requests) > 0)
                        @foreach($requests as $request)
                            <tr>
                                <td>{{$request->user->firstname}}</td>
                                <td>{{$request->user->lastname}}</td>
                                <td>
                                    <button class="btn btn-success" onclick="approval({{$request->id}}, 1)"><i class="fas fa-check"></i></button>
                                    <button class="btn btn-danger" onclick="approval({{$request->id}}, 2)"><i class="fas fa-times"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready( function () {
        $('#users_table').DataTable();
    } );

    function approval(id, action){
        let form = new FormData;
        form.append('action', action);
        axios.post('/admin/users/pending/'+id, form)
        .then(function(response){
            if(response.data.status){
                toastr.success(response.data.message)
                setTimeout(function(){
                    window.location.pathname = '/admin/users';
                }, 3000);
            }else{
                toastr.danger(message)
            }
        })
        .catch(function(error){
            console.log(error)
        })
    }
</script>
@endsection