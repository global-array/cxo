@extends('layouts.participant')

@section('css')
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header h2">
                    Participants
                </div>
                <div class="card-body">
                    <view-participant></view-participant>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
@endsection