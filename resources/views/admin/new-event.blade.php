@extends('layouts.main')

@section('css')
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Events
            <a class="btn btn-primary float-right" href="{{route('admin.events')}}">Back</a>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    
                    <div class="form-group">
                        <label for="eventname">Event Name</label>
                        <input type="text" class="form-control" id="eventname" placeholder="Event Name">
                    </div>

                    <div class="form-group">
                        <label for="eventdate">Event Date</label>
                        <input type="text" class="form-control" id="eventdate" name="eventdate" value="10/24/1984" />
                    </div>
                    
                    <div class="form-group">
                        <label for="registrationDates">Registration Dates</label>
                        <input class="form-control" id="registrationDates" type="text" name="registrationDates" value="01/01/2018 - 01/15/2018" />
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="eventImage">Upload event image</label>
                        <input type="file" class="form-control-file" id="eventImage">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="editor">Event Details</label>
                        <div id="editor" style="height: 300px"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script>
    var quill = new Quill('#editor', {
        theme: 'snow'
    });

    function getdata(){
        console.log(quill.getContents());
    }

    $('input[name="registrationDates"]').daterangepicker();
    $('input[name="eventdate"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
    });
</script>
@endsection