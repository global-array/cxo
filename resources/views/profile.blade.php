@extends('layouts.app')

@section('css')
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header h3">Profile</div>

                <div class="card-body">
                    <profile-component :user-auth="{{Auth::user()}}" :user-activities="{{$activities}}"></profile-component>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
@endsection