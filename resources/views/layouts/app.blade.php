<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}"> -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
    <link rel="manifest" href="{{asset('manifest.json')}}">
    @yield('css')
    @laravelPWA
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-dark shadow-sm">
            <div class="container text-white">
                <a class="navbar-brand text-white" href="{{ url('/home') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link text-white" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            {{-- <li class="nav-item">
                                <a class="nav-link text-white" href="{{ route('activities') }}">{{ __('Activities') }}</a>
                            </li> --}}
                            <li class="nav-item">
                                <a class="nav-link text-white" href="{{ route('events') }}">{{ __('Events') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-white" href="{{ route('profile') }}">{{ __('Profile') }}</a>
                            </li>
                            @if(Auth::user()->permission != 1 && Auth::user()->email_verified_at)
                                <li class="nav-item">
                                    <a class="nav-link btn btn-success my-2 my-sm-0 text-white" href="#" data-toggle="modal" data-target="#sponsorModal">
                                        Become a sponsor
                                    </a>
                                    
                                    <div class="modal fade" id="sponsorModal" tabindex="-1" role="dialog" aria-labelledby="sponsorModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title text-dark" id="sponsorModalLabel">Become a sponsor</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body text-dark">
                                            
                                                <div class="alert alert-danger d-none" id="sponsorAlert" role="alert"><ul></ul></div>

                                                <div class="form-group">
                                                    <label for="eventdate">Set Appointment</label>
                                                    <input type="text" class="form-control" id="appointment" name="appointment"/>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="eventImage">Upload Valid ID</label>
                                                    <input type="file" class="form-control-file" id="validid"  accept="image/*">
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                <button type="button" class="btn btn-primary" onclick="applySponsor({{Auth::id()}})">Submit</button>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endif
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->email }}
                                @if(Auth::user()->permission == 1)
                                    <span class="badge badge-success" style="font-size: 90%;">Sponsor</span>
                                @endif
                                <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    @yield('js')
    <script>
        var today = new Date(); 
        var dd = today.getDate(); 
        var mm = today.getMonth()+1; //January is 0! 
        var yyyy = today.getFullYear(); 
        if(dd<10){ dd='0'+dd } 
        if(mm<10){ mm='0'+mm } 
        var today = dd+'/'+mm+'/'+yyyy;
        $('input[name="appointment"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minDate: moment().add(3, 'd')
        });

        function applySponsor(id){
            let validid = document.querySelector( '#validid' );
            let form = new FormData();
            form.append('_token', $('input[name=_token]' ).val());
            form.append('appointment', $('#appointment').val());
            form.append('validid', validid.files[0]);
            
            axios.post('/apply-sponsor/'+id, form)
            .then(function(res){
                console.log(res)
                if($.isEmptyObject(res.data.error)){
                    if(res.data.status == 201){
                        toastr.success(res.data.message)
                        setTimeout(() => {
                            window.location = '/events'
                        }, 3000);
                    }else{
                        toastr.error(res.data.message)
                    }
                }else{
                    $('#sponsorAlert').removeClass('d-none');
                    var errors = res.data.error;
                    errors.forEach(function(error){
                        $('#sponsorAlert ul').append('<li>'+error+'</li>')
                    })
                }
            })
            .catch(function(error){
                console.log(error)
            })
        }
    </script>
</body>
</html>
