@extends('layouts.main')

@section('style')
@endsection

@section('content')
<div class="container">
    <div class="card">
    	<div class="card-header h2">Dashboard</div>
        <div class="card-body row">
            <div class="col-md-6 col-sm-12 col-lg-6 pt-3">
                <h3>Upcoming Events</h3>
                <div class="list-group">
                    @if(count($events) >= 0)
                        @foreach($events as $event)
                            <a href="/admin/events/{{$event->id}}" class="list-group-item list-group-item-action">
                                {{$event->title}}
                                <span class="badge badge-primary badge-pill float-right">{{$event->date}}</span>
                            </a>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-lg-6 pt-3">
                <h3>Total Number of Accounts: {{$all}}</h3>
                <canvas id="myChart" width="400" height="200"></canvas>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ['Sponsors', 'Users'],
        datasets: [{
            label: '# of Votes',
            data: ['{{$totalsponsors}}','{{$totalusers}}'],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
            ],
            borderWidth: 1
        }]
    }
});
</script>
@endsection