<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" >
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <link rel="manifest" href="{{asset('manifest.json')}}">
    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="PWA">
    <link rel="icon" sizes="512x512" href="{{asset('/images/icons/icon-512x512.png')}}">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="PWA">
    <link rel="apple-touch-icon" href="{{asset('/images/icons/icon-512x512.png')}}">
    <link href="/images/icons/splash-640x1136.png" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/images/icons/splash-750x1334.png" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/images/icons/splash-1242x2208.png" media="(device-width: 621px) and (device-height: 1104px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />
    <link href="/images/icons/splash-1125x2436.png" media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />
    <link href="/images/icons/splash-828x1792.png" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/images/icons/splash-1242x2688.png" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />
    <link href="/images/icons/splash-1536x2048.png" media="(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/images/icons/splash-1668x2224.png" media="(device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/images/icons/splash-1668x2388.png" media="(device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/images/icons/splash-2048x2732.png" media="(device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    
    <script>
    new WOW().init();
    </script>
    <title>CXO Project | Home</title>
    @laravelPWA
</head>
<body>
    <!-- navigation bar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top"  id="banner">
        <button class="navbar-toggler order-first" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon "></span>
        </button>
        <a class="navbar-brand" href="#"><img style="width: 200px; height:55px;" src="{{asset('img/cxo_logo.png')}}" alt=""></a>
        <div class="collapse navbar-collapse " id="navbarSupportedContent">
          <ul class="navbar-nav  text-center ">
            <li class="nav-item">
              <a class="nav-link" href="#about">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#news">News</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#accordion">Events</a>
            </li>
          </ul>
          @if (Route::has('login'))
          
            <ul class="navbar-nav  text-center ml-auto">
                @auth
                    <li class="nav-item">
                        <a  class="nav-link" href="{{ url('/home') }}">Home</a>
                    </li>
                @else
                    <li class="nav-item">
                        <a  class="nav-link" href="{{ route('login') }}">Login</a>
                    </li>

                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a  class="nav-link" href="{{ route('register') }}">Register</a>
                        </li>
                    @endif
                @endauth
            </ul>
        @endif
        </div>
    </nav>
    <!-- navigation bar -->
    <header>
        <div class="overlay"></div>
        <video class="embed-responsive-item" playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
            <source src="https://www.vidsplay.com/wp-content/uploads/2017/05/peoplenyc.mp4?_=1" type="video/mp4">
        </video>
        <div class="container h-100">
            <div class="d-flex h-100 text-center align-items-center">
            <div class="w-100 text-white">
                <h1 class="display-4">CXO|<sub>Project</sub></h1>
                <p class="lead mb-0"></p>
            </div>
            </div>
        </div>
    </header>
    
    <div class="container-fluid">
        <div class="row">
    <section class="bg-dark bground" id="about">
        <div class="text-intro text-light container-fluid" >
            <div class="col-md">
            <h2 class="text-center wow fadeIn" data-wow-duration="1s">Configure your growth with us.</h2>
            <p class=" wow fadeIn" data-wow-duration="4s">CXO Project  flesh out strategic event concepts that meets our client's objectives  and helps them to uncover growth opportunities. <br><br>
                With our expertise in market research combined with technology - we partner with our clients from start to finish, focusing on their needs while producing new ideas and developing effective strategies. <br><br>
                Our Geo Expertise covers South East Asia (Singapore, Indonesia, Malaysia, Philippines, Thailand, Vietnam) and Japan. <br><br>
                Our mission is to deliver strategic event platform tailored-fit to your business needs and requirement that helps you gain a competitive edge in the market.
            </p>
            </div>
        </div>
            <section>
              <svg id="curve" data_name="Layer 1" xmlns="http://www.w3.org/2000/svg/" viewBox="0 0 1416.99 174.01">
                <path class="cls-1" d="M0,280.8S283.66,59,608.94,163.56s437.93,150.57,808,10.34V309.54H0V280.8Z" transform="translate(0 -135.53)"/>
              </svg> 
            </section>
    </section>
      <div class="col-md-4 py-2 wow zoomIn " data-wow-duration="1s">
        <div class="card card-body border-default h-100 d1" style="box-shadow: 0 10px 15px rgba(0, 0, 0, .2); ">
            <h5 class="card-title text-center text-warning">Measure your results</h5>
            <p class="card-text">We examine what organizations are doing to stay relevant and competitive in this fast-paced world, and which ones are doing it best. We then strategize using smart tools and global resources in order to understand the implications of every choice our clients can make. </p>
        </div>
    </div>
    <div class="col-md-4 py-2 wow zoomIn " data-wow-duration="2s">
        <div class="card card-body h-100 d2" style="box-shadow: 0 10px 15px rgba(0, 0, 0, .2); ">
            <h5 class="card-title text-center text-success">Business Intelligence</h5>
            <p class="card-text">Get the valuable information that meets your objectives. Our tactical approach to primary research provides you comprehensive insights to help you make strategic decisions.</p>
        </div>
    </div>
    <div class="col-md-4 py-2 wow zoomIn " data-wow-duration="3s">
        <div class="card card-body h-100 d3" style="box-shadow: 0 10px 15px rgba(0, 0, 0, .2); ">
            <h5 class="card-title text-center  text-info">More than a connection</h5>
            <p class="card-text">Building relationship is our business.  Our community of key experts and leaders technology industry creates an opportunity for you to engage remarkably.</p>
        </div>
    </div>
    
    <div class="container mt-40 " id="news">
        <div class="row mt-30 pt-5 mb-5">
          @if(count($news) > 0)
            @foreach($news as $row)
              <div class="col-md-4 col-sm-6">
                <div class="card" style="width: 20rem;">
                    <img src="/img/news/{{$row->image}}" class="card-img-top" alt="{{$row->image}}">
                    <div class="card-body">
                      <a href="#" class="card-title h5" data-toggle="modal" data-target="#eventModal{{$row->id}}">{{$row->title}}</a>

                      <div class="modal fade" id="eventModal{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">{{$row->title}}</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="card">
                                <img src="/img/news/{{$row->image}}" class="card-img-top" alt="{{$row->image}}">
                                <div class="card-body">
                                  <p class="card-text">{!! $row->body !!}</p>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                            </div>
                          </div>
                        </div>
                      </div>
                      
                    </div>
                </div>
              </div>
            @endforeach
          @endif
        </div>
    </div>
    
    <div id="accordion" class="myaccordion col-md-12">        
        <div class="card wow slideInLeft" data-wow-duration="1s">
          <div class="card-header bg-info" id="headingOne">
            <h2 class="mb-0">
              <button class="d-flex align-items-center justify-content-between btn btn-link "  data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                <p class="text-light"> EVENT PROJECT MANAGEMENT <i class="fa fa-lightbulb-o" style="font-size:36px;"></i> </p>
                <span class="fa fa-stack fa-sm">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-minus fa-stack-1x fa-inverse"></i>
                </span>
              </button>
            </h2>
          </div>
          <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body bodey">
              <ul>
                <p>Our team will work with you from start to finish to flesh out strategic event concepts that is suitable to your objectives. Our expertise includes:</p>
                <li>Market Research</li>
                <li>Speaker/Delegate/Sponsorship Acquisitions </li>
                <li>Venue Selection</li>
                <li>Lead generation</li>
                <li>On-site team</li>
                <li>Event App </li>
              </ul>
            </div>
          </div>
        </div>

        <div class="card wow slideInLeft " data-wow-duration="2s">
          <div class="card-header bg-warning" id="headingTwo">
            <h2 class="mb-0">
              <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                <p class="text-light">STRATEGIC EVENT PLATFORM <i class="fa fa-comments-o" style="font-size:36px;"></i></p> 
                <span class="fa fa-stack fa-2x">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-plus fa-stack-1x fa-inverse"></i>
                </span>
              </button>
            </h2>
          </div>
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
            <div class="card-body bodey">
              <ul>
                <p>We produce exclusive meeting platforms that delivers a remarkable learning experience and provide an opportunity to accelerate business relationship. <br><br> Focus Areas:</p>
                <li>Cyber Security</li>
                <li>Cloud Technology</li>
                <li>Digital Transformation</li>
              </ul>
            </div>
          </div>
        </div>

        </div>




    </div><!--ROW-->
    </div><!--CONTAINER-FLUID-->
    <!--Footer-->
    <footer class="text-center">
        <div class=" bg-dark py-3">
          <div class="container">
            <div class="row py-3">
              <div class="col-md-6">
                <p class="text-white"><img style="width: 200px; height:55px;" src="{{asset('img/cxo_logo.png')}}" alt=""></p>
              </div>
              <div class="col-md-6">
                <div class="d-inline-block">
                  <div class="bg-circle-outline d-inline-block">
                    <a href="#" class="text-white"><i class="fa fa-2x fa-fw fa-facebook"></i>
              </a>
                  </div>
      
                  <div class="bg-circle-outline d-inline-block">
                    <a href="#" class="text-white">
                      <i class="fa fa-2x fa-fw fa-twitter"></i></a>
                  </div>
      
                  <div class="bg-circle-outline d-inline-block">
                    <a href="#" class="text-white">
                      <i class="fa fa-2x fa-fw fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    <!--Footer-->



<!--CDN for bootsrap 4.1.3 -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
<!--CDN for bootsrap 4.1.3 -->
<script src="https://account.snatchbot.me/script.js"></script><script>window.sntchChat.Init(72089)</script> 
</body>
</html>
<script>

	$(document).on("scroll", function(){
		if
      ($(document).scrollTop() > 86){
		  $("#banner").addClass("shrink");
		}
		else
		{
			$("#banner").removeClass("shrink");
		}
	});

  $("#accordion").on("hide.bs.collapse show.bs.collapse", e => {
  $(e.target)
    .prev()
    .find("i:last-child")
    .toggleClass("fa-minus fa-plus");
});

</script>


<style>
       @import url('https://fonts.googleapis.com/css?family=Gothic+A1|Kaushan+Script|Libre+Baskerville|Lobster');

	h1{
        text-transform:uppercase;}
.navbar-brand  span{
	color: #fed136;
	font-size:25px;
    font-weight:700;
    letter-spacing:0.1em;
    font-family: 'Kaushan Script','Helvetica Neue',Helvetica,Arial,cursive;
    
}

.navbar-nav .nav-item .nav-link{
	padding: 1.1em 1em!important;
	font-size: 120%;
    font-weight: 500;
    letter-spacing: 1px;
    color: #fff;
   font-family: 'Gothic A1', sans-serif;
}
.navbar-nav .nav-item .nav-link:hover{color:#fed136;}
.navbar-expand-md .navbar-nav .dropdown-menu{
	border:3px solid #fed136; 
}
.dropdown-item:hover{background-color:#fed136;color:#fff;}
nav{
    -webkit-transition: padding-top .3s,padding-bottom .3s;
    -moz-transition: padding-top .3s,padding-bottom .3s;
    transition: padding-top .3s,padding-bottom .3s;
    border: none;
	}
	
 .shrink {
    padding-top: 0;
    padding-bottom: 0;
    background-color: #212529;
}
/* mobile view */
@media (max-width:990px)
{
	.navbar-nav{
		background-color:#000;
		border-top:3px solid #fed136;
		color:#fff;
		z-index:1;
		margin-top:5px;
		}
	.navbar-nav .nav-item .nav-link{
	padding: 0.7em 1em!important;
	font-size: 100%;
    font-weight: 500;
    }
.banner-sub-heading{
	font-size: 10px;
    font-weight: 200;
    line-height: 10px;
    margin-bottom: 40px;
}

}

@media (max-width:990px){
	.banner-text{
	padding:150px 0 150px 0;
}
	.banner-sub-heading{
	font-size: 23px;
    font-weight: 200;
    line-height: 23px;
    margin-bottom: 40px;
}

}

header {
  position: relative;
  background-color: black;
  height: 75vh;
  min-height: 25rem;
  width: 100%;
  overflow: hidden;
  
  
}

header video {
  position: absolute;
  top: 50%;
  left: 50%;
  min-width: 100%;
  min-height: 100%;
  width: auto;
  height: auto;
  z-index: 0;
  -ms-transform: translateX(-50%) translateY(-50%);
  -moz-transform: translateX(-50%) translateY(-50%);
  -webkit-transform: translateX(-50%) translateY(-50%);
  transform: translateX(-50%) translateY(-50%);
}

header .container {
  position: relative;
  z-index: 2;
  
}

header .overlay {
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  background-color: rgb(97, 96, 96);
  opacity: 0.5;
  z-index: 1;
}


@media(min-width:990px){
        .navbar-brand{
            position: absolute;
            left: 50%;
            transform:translateX(-50%);

        }
    }

    .bground{
        background: linear-gradient(45deg,#8614ce,#ff0057);
        width: 100%;
      
    }

    #curve{
      width: 100%;

    }
    #curve path{
      fill: #fff;
    }

    .text-intro{
        padding-top: 5%;
        text-align: center;
        padding-bottom: 5%;
    }
    .d1,.d2,.d3{
      background-color:#fff;
    }
    .card-text{
      text-indent: 2em;
    }
    
.myaccordion {
  margin: 50px auto;
}

.myaccordion .card,
.myaccordion .card:last-child .card-header {
  border: none;
}

.myaccordion .card-header {
  border-bottom-color: #EDEFF0;
  background: transparent;
}

.myaccordion .fa-stack {
  font-size: 18px;
}

.myaccordion .btn {
  width: 100%;
  font-weight: bold;
  color: #004987;
  padding: 0;
}

.myaccordion .btn-link:hover,
.myaccordion .btn-link:focus {
  text-decoration: none;
}

.myaccordion li + li {
  margin-top: 10px;
}
/*********************** Demo - 16 *******************/
.box16{
  text-align:center;
  color:#fff;
  position:relative;
  }
.box16 .box-content,.box16:after{
  width:100%;
  position:absolute;
  left:0;
  }
.box16:after{
  content:"";
  height:100%;
  background:linear-gradient(to bottom,rgba(0,0,0,0) 0,rgba(0,0,0,.08) 69%,rgba(0,0,0,.76) 100%);
  top:0;transition:all .5s ease 0s;
  }
.box16 .post,.box16 .title{
  transform:translateY(145px);
  transition:all .4s cubic-bezier(.13,.62,.81,.91) 0s;
  }
.box16:hover:after{
  background:linear-gradient(to bottom,rgba(0,0,0,.01) 0,rgba(0,0,0,.09) 11%,rgba(0,0,0,.12) 13%,rgba(0,0,0,.19) 20%,rgba(0,0,0,.29) 28%,rgba(0,0,0,.29) 29%,rgba(0,0,0,.42) 38%,rgba(0,0,0,.46) 43%,rgba(0,0,0,.53) 47%,rgba(0,0,0,.75) 69%,rgba(0,0,0,.87) 84%,rgba(0,0,0,.98) 99%,rgba(0,0,0,.94) 100%)
  }
.box16 img{
  width:100%;
  min-height:350px;
  }
.box16 .box-content{
  bottom:0;
  z-index:1;
  }
.box16 .title{
  font-size:22px;
  font-weight:700;
  text-transform:uppercase;
  margin:0 0 10px;
  }
.box16 .post{
  display:block;
  padding:8px 0;
  font-size:15px;
  }
.box16 .social li a,.box17 .icon li a{
  font-size:16px;
  color:#fff;
  }
.box16:hover .post,.box16:hover .title{
  transform:translateY(0);
  }
.box16 .social{
  list-style:none;
  padding:0 0 5px;
  margin:40px 0 25px;
  opacity:0;
  position:relative;
  transform:perspective(500px) rotateX(-90deg) rotateY(0) rotateZ(0);
  transition:all .6s cubic-bezier(0,0,.58,1) 0s;
  }
.box16:hover .social{
  opacity:1;
  transform:perspective(500px) rotateX(0) rotateY(0) rotateZ(0);
  }
.box16 .social:before{
  content:"";
  width:50px;
  height:2px;
  background:#fff;
  margin:0 auto;
  position:absolute;
  top:-23px;
  left:0;
  right:0;
  }
.box16 .social li{
  display:inline-block;
  }
.box16 .social li a{
  display:block;
  margin-right:10px;
  transition:all .3s ease 0s;
  text-decoration: none; }
.box17 .icon li,.box17 .icon li a{
  display:inline-block;
  }
.box16 .social li:last-child a{
  margin-right:0;
}
@media only screen and (max-width:990px){
  .box16{
    margin-bottom:30px;
}
}

</style>