<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->string('image')->nullable();
            $table->string('date');
            $table->string('venue');
            $table->string('registration_from');
            $table->string('registration_to');
            $table->integer('max_participants');
            $table->integer('approval')->comment('0 - pending; 1 - allow; 2 - deny;');
            $table->string('sponsors')->nullable()->comment('array of sponsors');
            $table->string('request_sponsor')->comment('0 - no; 1 - yes;');
            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
